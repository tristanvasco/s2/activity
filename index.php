<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s2: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
		<h1>Repetition Control Structures Activity</h1>

		<h3>Divisible by five</h3>
		<?php modifiedForLoop(); ?>

		<h3>push</h3>
		<?php array_push($students, 'Hillary'); ?>
		<?php print_r($students); ?>

		<h3>count</h3>
		<p><?php echo count($students); ?></p>

		<h3>push again and count</h3>
		<?php array_push($students, 'Rupert'); ?>
		<?php print_r($students); ?>
		<p><?php echo count($students); ?></p>

		<h3>remove first student and count</h3>
		<?php array_shift($students); ?>
		<?php print_r($students); ?>
		<p><?php echo count($students); ?></p>


</body>
</html>